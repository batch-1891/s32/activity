let http = require('http')

let port = 4000

const server = http.createServer(function(request, response) {
	// request.method is used to get the HTTP method that is currently used by the browser to send a request
	if(request.url == '/' && request.method == 'GET'){ //In this case, we are checking if the current request method is a GET HTTP method
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Welcome to Booking System')
	}

	if(request.url == '/profile' && request.method == 'GET'){ //In this one, we are checking if the current request method is a POST HTTP method
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Welcome to your profile')
	}

	if(request.url == '/courses' && request.method == 'GET'){ //In this one, we are checking if the current request method is a POST HTTP method
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end(`Here's our courses availabe`)
	}

	if(request.url == '/addCourse' && request.method == 'POST'){ //In this one, we are checking if the current request method is a POST HTTP method
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Add course to our resourses')
	}

})
server.listen(port)

console.log(`Server is running at localhost:${port}`)
